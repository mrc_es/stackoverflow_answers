#!/bin/bash

#-----------------------------------------------------------------#
#  Title:
#  ¿Que significa “// /” dentro de un if que niega una variable?  #
#
#-----------------------------------------------------------------#

var1="aEEaaEEaaEEa"; echo "${var1/a/0}"

var1="aEEaaEEaaEEa"; echo "${var1//a/0}"

var1="a a"; [ "${var1// /}" ] && echo true || echo fals

var1="  "; [ "${var1// /}" ] && echo true || echo false

var1="a a"; [ ! "${var1// /}" ] && echo true || echo false