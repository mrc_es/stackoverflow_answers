#!/bin/awk -f
BEGIN{FS=",";OFS=","}
{
    if($8 == 1)
        $8="USA"
    else if($8 == 2)
        $8="Europe"
    else if($8 == 3)
        $8="Asia";
    print $0
}
