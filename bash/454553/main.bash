#!/bin/bash

echo "Creacción de un Sistema de Ficheros"

echo "Veamos la información completa de las particiones disponibles"
sudo lsblk -fm

echo "Ponga simplemente el nombre de la partición elegida, ignore la ruta (ejem.: sda1, sdb3, sdc5): "
read unidad

cat << __
Elija el número correspondiente al tipo de sistema de archivos elegido:
* Para sistema ext2 ........... ponga 2
* Para sistema ext3 ........... ponga 3
* Para sistema ext4 ........... ponga 4
__
read sistema

if [[ ! "$sistema" =~ ^(1|2|3|4)$ ]]
then
    echo "Opción \"${sistema}\" inválida"
    exit 1
fi

echo "Elija un nombre de etiqueta para el Sistema de Ficheros: "
read etiqueta

declare dispositivo="/dev/$unidad"
mkfs -t "ext${sistema}" -L "$etiqueta" "$dispositivo"

[[ "$?" == 0 ]] && echo "El sistema de Ficheros ha sido creado con éxito"

echo "Para su información, nos encontramos en la siguiente ruta del sistema"
pwd

echo "Ponga a continuación la ruta absoluta con el directorio donde desea montar el Sistema de Ficheros"
read directorio

mkdir -p "$directorio"
sudo mount "$dispositivo" "$directorio"
