[Enlace a la pregunta](https://es.stackoverflow.com/questions/454553/script-en-linux-que-no-se-continuar-sobre-creación-de-sistema-de-archivos#454563)

Antes de correr el programa, preparé dos archivos con `$ truncate -s 10M disco1` y posteriormente los mapee a algún dispositivo `/dev/loopN` con `$ losetup /dev/loopN /tmp/formatos/puntoN`.

De esta manera, ya podrán ser utilizados por `mkfs`.

## Modo de correr

```bash
$ chmod +x ./main.bash
$ ./main.bash
```

Resultando:

<img src="./resultado.PNG" style="zoom: 67%;" />