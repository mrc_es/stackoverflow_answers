#!/bin/bash

readonly directorio=/directorio/dir
readonly archivos=./archivos.txt

awk -v inicio=1 -v fin=5 'NR>inicio && NR<fin' "$archivos" \
    | xargs -P 0 -I {} echo "cp {} $directorio"

