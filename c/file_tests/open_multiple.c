#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* read_file(FILE* file) {
    char *content;
    size_t n = 0;
    int c;

    if (file == NULL)
        return NULL;  //could not open file

    content = malloc(1000);

    while ((c = fgetc(file)) != EOF)
    {
        content[n++] = (char) c;
    }

    content[n] = '\0';        

    return content;
}

#define HEADER_TEMPLATE "===== \%s ====="

char* concat_files(char* c1, char* filename, char* c2) {
  char* header = (char*) malloc(strlen(HEADER_TEMPLATE) + strlen(filename));

  sprintf(header, HEADER_TEMPLATE, filename);

  int new_size = strlen(c1) + strlen(c2) + strlen(header) + 1;
  char* buf = (char*) malloc(new_size);

  strcpy(buf, c1);
  strcat(buf, header);
  strcat(buf, "\n");
  strcat(buf, c2);

  free(header);
  return buf;
}

#define E_FILE_NOT_FOUND 1

int main(int argc, char** argv) {
  char* concatenated = "";
  int _char;
  for (int param = 1; param < argc; param++) {
    char* filename = argv[param];
    FILE* file = fopen(filename, "r");

    if (file == NULL) {
      fprintf(stderr, "File \"%s\" doesn't exist\n", filename);
      exit(E_FILE_NOT_FOUND);
    }
      
    concatenated = concat_files(concatenated, filename, read_file(file));
    fclose(file);
  }

  printf("%s", concatenated);
  free(concatenated);
  exit(0);
  return 0;
}
