#!/usr/bin/env python3

def print_level(level):
    for number in range(1, level + 1):
        print(number, end=" ")
    print()

def print_levels(levels):
    for level in range(1, levels + 1):
        print_level(level)

if __name__ == '__main__':
    LEVELS = 5

    print_levels(LEVELS)

